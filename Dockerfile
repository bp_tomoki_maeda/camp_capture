FROM python:3.6.9

WORKDIR /home/pi/campcapture

COPY . .

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt-get install -y nodejs

RUN curl -o- -L https://yarnpkg.com/install.sh | bash

RUN pip install -U pip && pip install -r requirements.txt

WORKDIR hubot/slack_mention

RUN $HOME/.yarn/bin/yarn install

WORKDIR /home/pi/campcapture
