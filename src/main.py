#!/usr/local/env python
import argparse
from datetime import datetime
import io
import os
import subprocess
import sys

from google.cloud import vision
from google.cloud.vision import types
from PIL import Image, ImageDraw
import requests


def main():
    """
    Upload an image file to camp_2019_as channel in Slack

    """

    file = capture()

    out = file.split('.')
    output_filename = out[0] + '_out.' + out[1]

    with open(file, 'rb') as image_file:
        objects = localize_objects(image_file)
        image_file.seek(0)
        highlight_objects(image_file, objects, output_filename)

    files = {'file': open(output_filename, 'rb')}
    param = {'token': 'xoxb-324599662128-665666943972-XCLCnKIbxynbqNZkFAQt2guz',
             'channels': 'CK8L48LR0',
             'name':'a current scene',
             'title':'current situation'}
    reslt = requests.post(url='https://slack.com/api/files.upload',
                          params=param,
                          files=files)

    return 0

def capture():
    """
    Take a picture into a given path.

    Return
    ------
    path : A path to a captured image.
    """

    now = datetime.now().strftime('%Y%m%d_%H%M')
    path = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                        '../image/') + now + '.jpg')
    commands = ["fswebcam", "-F", "2", "--flip", "h,v",
                "-r", "640x480", "--no-banner", path]
    subprocess.run(commands, stderr=subprocess.DEVNULL)
    return path

def localize_objects(image_file):
    """Localize objects in the local image.

    Args:
    image_file: A file-like object of an image.

    Returns:
    objects: Objects found in the image.
    """
    from google.cloud import vision
    client = vision.ImageAnnotatorClient()

    image = vision.types.Image(content=image_file.read())

    annotations = client.object_localization(
        image=image).localized_object_annotations

    objects = []
    for annotate in annotations:
        if annotate.name == 'Person':
            objects.append(annotate)

    print('There would be {} person{}.'.format(
        'no' if len(objects) == 0 else len(objects),
        '' if len(objects) <= 1 else 's'
    ))

    return objects

def highlight_objects(image_file, objects, output_filename):
    """Draws a polygon around the objects, then saves to output_filename.

    Args:
      image_file: a file containing the image.
      objects: a list of objects found in the file. This should be in the format
          returned by the Vision API.
      output_filename: the name of the image file to be created, where the
          objects have polygons drawn around them.
    """
    im = Image.open(image_file)
    draw = ImageDraw.Draw(im)

    for object in objects:
        box = [(int(vertex.x * im.width), int(vertex.y * im.height))
               for vertex in object.bounding_poly.normalized_vertices
               if object.name == 'Person']
        draw.line(box + [box[0]], width=4, fill='#00ff00')

    im.save(output_filename)

if __name__ == '__main__':
    sys.exit(main())
