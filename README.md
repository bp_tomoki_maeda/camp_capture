# camp_capture
Ack slack mention, capture image by a camera, and push image to slack.

## How to use Hubot

### install nodejs packages
cd hubot/slack_mention

npm install

or

yarn add

### run hubot on local
./bin/hubot

### connect hubot to slack
./slack_adapter
