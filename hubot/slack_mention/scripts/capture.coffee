module.exports = (robot) ->
  robot.hear /capture/, (msg) ->
    @exec = require('child_process').exec
    command = "cd /home/pi/campcapture && bin/execute.sh"
    @exec command, (error, stdout, stderr) ->
      msg.send error if error?
      msg.send stdout if stdout?
      msg.send stderr if stderr?
    msg.reply 'Captureing done.'
